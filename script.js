function showFile(element) {
	console.log(element.firstElementChild.src);
	overlay.style.backgroundImage = 'url("'+element.firstElementChild.src+'")';
	overlay.style.display = 'block';
	overlay.onclick = function(e) {
		e.target.style.display = 'none';
	}
}

function formatSize(n) {
	if (typeof(n) == "undefined") return "Hier sollte nichts stehen.";
	let ndl = 1000; // number delimiter
	let sdl = 1024; // size delimiter
	let s = ['Bytes','K','M','G','T','P','E','Z','Y'];
	let slast = s.length-1;
	let i = 0;
	let n2 = n;
	while (Math.abs(n2) >= ndl && i < slast) {
		n2 /= sdl;
		i++;
	}
	return n2.toFixed(i ? 2:0) +' '+ ( Math.abs(n) == 1 ? 'Byte':s[i] ) + (i ? 'B':'');
}

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function update_name() {
	var a = document.querySelector('input[name=name]').value;
	var b = document.querySelector('input[name=text]').value;
	localStorage.username_msgFI = a;
	localStorage.usermsg_msgFI = b;
	
	if (a.trim() != "") {
		info.innerText = "";
		return true;
	} else {
		info.innerText = "Bitte das Formular ausfüllen.";
	}
	document.querySelector('input[name=name]').value = localStorage.username_msgFI;
	document.querySelector('input[name=text]').value = localStorage.usermsg_msgFI;
}

function notifyMe(msg) {
	Push.create('New Message!', {
		// icon: 'favicon.ico',
		// timeout: 4000,
		body: msg.name + ":\n" + msg.msg,
		link: '/#',
		onClick: function () {
			console.log("Looking!");
			window.focus();
			this.close();
		},
		vibrate: [250, 100, 100, 250, 250]
	});
	Push.clear();
}

function show_file_names() {
	if (filein.value) {
		filename.innerText = "Datei: " + filein.value.replace("C:\\fakepath\\", "");
	}
	else filename.innerText = "Keine Datei ausgewählt";
}

function shall_update(m) {
	var other = 0;
	
	var last_msg = m[0] ? m[0] : {"id": 0};
	if (last_msg.id > msgId || msgId == -1) {
		if (!sent) {
			notifyMe(last_msg);
			other = 2;
		}
		else other = 1;
		msgId = parseInt(last_msg.id);
	}
	return other;
	// Rückgabe "other" kann (wahrscheinlich) weg gelassen werden.
}

function update() {
	fetch("ausgabe.php").then(data=>data.json()).then(messages=>{
		var err = messages[messages.length-1];
		if (err && err.error) {
			console.log(err);
			messages.pop();
		}
		
		messages.reverse();
		for (var message of messages) {
			if (msgId < message.id) {
				var eintrag = document.createElement('div');
				eintrag.className = "eintrag";
				var text = "<div>| Eintrag-Nr.: <b>" + formatNumber(message.id) + "</b> |";
				// To-do: Prüfe, ob Benutzer an jemand bestimmten (nur einer oder Gruppe) geschrieben hatte.
				text += "<br />Benutzer <a href='#' name='username'><b>" + message.name + "</b></a> schrieb";
				text += " am<br />" + message.datum + " um " + message.uhr + " Uhr </div><a href='#' name='usermsg'><p>" + message.msg + "</p></a>";
				if(message.files.length) {
					var i = message.ftypes[0].length;
					text += "<div class='filecnt'>Anhang: " + (i == 1 ? "1 Datei" : i+" Dateien") + "</div>";
					text += "<div class='filediv'>";
					
					var i = 0;
					for(var file of message.files) {
						var ftag = message.ftags[i];
						text += "<div class='files'>";
						text += "<div class='filename'>" + file.slice(8);
						text += "<br />Größe: "+ formatSize(message.fsizes[i]);
						var ftyp = message.ftypes[0][i];
						if (!ftyp.startsWith("application") && !ftyp.endsWith("html")) {
							text += '<div class="thumbnail" onclick="showFile(this)"> <'+ftag+' controls src=\"'+file+'\" alt=\"'+file.slice(8)+'\"></'+ftag+'> </div>';
						}
						text += "</div>";
						text += '<a href=\"'+file+'\" download> <button class="dlb">&#x2B07</button> </a>';
						text += "</div>";
						
						i++;
					}
					text += "</div>";
				}
				
				eintrag.innerHTML = text;
				ausgabe.appendChild(eintrag);
			}
		}
		shall_update(messages.reverse());
		sent = 0;
		
		if (box.checked) try {
			document.querySelector('.eintrag:last-of-type').scrollIntoView();
		} catch(e) {
			if (document.querySelectorAll('.eintrag').length === 0) {
				info.innerText = "Schreibe die erste Nachricht.";
			} else {
				info.innerText = "";
			}
		}
	});
}

// Definiere die localStorage-Werte
if (localStorage.username_msgFI == undefined) localStorage.username_msgFI = "";
if (localStorage.usermsg_msgFI == undefined) localStorage.usermsg_msgFI = "";

if (localStorage.username_msgFI.trim() == "") document.querySelector('input[name=name]').focus();
else document.querySelector('input[name=text]').focus();

filein.onchange = show_file_names;
document.querySelector('input[name=name]').onchange = update_name;
document.querySelector('input[name=text]').onchange = update_name;

document.querySelector('input[name=name]').value = localStorage.username_msgFI;
document.querySelector('input[name=text]').value = localStorage.usermsg_msgFI;

show_file_names();
update_name();

var msgId = -1;
var sent = 1;
var itv; // Intervall
addEventListener('load', e=>{ update(); itv = setInterval(update, 1500) });

onsubmit = function(e) {
	e.preventDefault();
	if (!update_name()) return;
	
	var formElement = document.querySelector('#eingabe');
	var form = new FormData(formElement);
	
	/* XHR HANDLING */
	var xhr = new XMLHttpRequest();
	xhr.open(formElement.method, '#'); // formElement.action
	xhr.onload = function() {
		// console.log(this.response);
	}
	xhr.onloadstart = function (event) {
		info.className = 'noblink';
	}
	xhr.onloadend = function (event) {
		info.className = '';
	}
	var s1 = Date.now();
	xhr.onprogress = function(event) {
		// Download
	}
	xhr.upload.onprogress = function(event) {
		// Upload
		if (event.lengthComputable) {
			var s2 = Date.now() - s1;
			var percent = event.loaded / event.total * 100;
			var trem = ((s2 / (percent / 100)) - s2) / 1000;
			info.innerText = "Upload-Dauer:\nNoch " + trem.toFixed(1) + "s";
			info.innerText += ' ('+percent.toFixed(1) + ' %)';
		}
	}
	xhr.send(form);
	/* END XHR HANDLING */
	
	fetch("eingabe.php", {
		method: 'POST',
		body: form
	})
	.then(data=>data.text())
	.then(text=>{
		console.log(text);
		if (!text) {
			info.innerText = "Bitte etwas schreiben!";
			return;
		} else if (text.endsWith("| Warte")) {
			info.innerText = "Bitte bis 5 Sekunden warten!";
			document.querySelector('input[name=text]').disabled = true;
			setTimeout(function() {
				info.innerText = "";
				document.querySelector('input[name=text]').disabled = false;
				document.querySelector('input[name=text]').focus();
			}, 1500);
			return;
		} else {
			info.innerText = "Gesendet.";
			sent = 1;
			setTimeout(function() { info.innerText = ""; }, 1000);
		}
		document.querySelector('input[name=text]').value='';
		update();
	});
}

function formatNumber(n, p = 0, ts = '.', dp = ',') {
	// Format a number n using: 
	//   p decimal places (two by default)
	//   ts as the thousands separator (comma by default) and
	//   dp as the  decimal point (period by default).
	//
	//   If p < 0 or p > 20 results are implementation dependent.
	
	var t = [];
	// Get number and decimal part of n
	n = Number(n).toFixed(p).split('.');
	
	// Add thousands separator and decimal point (if required):
	for (var iLen = n[0].length, i = iLen? iLen % 3 || 3 : 0, j = 0; i <= iLen; i+=3) {
		t.push(n[0].substring(j, i));
		j = i;
	}
	// Insert separators and return result
	return t.join(ts) + (n[1]? dp + n[1] : '');
}