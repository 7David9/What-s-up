<?php
// $pdo = new PDO('mysql:host=localhost; dbname=fi-messenger', 'root', '');
$pdo = new PDO('mysql:host=localhost; dbname=fi-messenger', 'David', 'aA1234Aa.');
$pdo->query("SET NAMES 'utf8mb4'");

$sql = $pdo->prepare("SELECT id, message, username, files, file_type, file_tag, file_size, DATE_FORMAT(date, '%d.%m.%Y') as datum, DATE_FORMAT(date, '%H:%i:%s') as uhr, to_group, to_user FROM messages ORDER BY id DESC LIMIT 1000");
$sql->execute();
$messages = [];

while($row = $sql->fetch()) {
	array_push($messages, array(
		"id" => htmlentities($row['id']),
		"name" => htmlentities($row['username']),
		"msg" => htmlentities($row['message']),
		"datum" => htmlentities($row['datum']),
		"uhr" => htmlentities($row['uhr']),
		"to_group" => htmlentities($row['to_group']),
		"to_user" => htmlentities($row['to_user']),
		"files" => json_decode($row['files']),
		"ftypes" => json_decode($row['file_type']),
		"ftags" => json_decode($row['file_tag']),
		"fsizes" => json_decode($row['file_size']),
	));
}
echo json_encode($messages);
?>