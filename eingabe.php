<?php
// $pdo = new PDO('mysql:host=localhost; dbname=fi-messenger', 'root', '');
$pdo = new PDO('mysql:host=localhost; dbname=fi-messenger', 'David', 'aA1234Aa.');
$pdo->query("SET NAMES 'utf8mb4'");

/* FILE HANDLING */

	$files = [];
	$ftags = [];
	$ftypes = [];
	$fsizes = [];
	$embed = null;
	
	$upload_dir = 'uploads/';
	if(!is_dir($upload_dir)){
		mkdir($upload_dir, 0777);
	}
	$myTypes = ["audio", "video", "image"];
	$myTags = ["audio", "video", "img"];
	
	foreach($_FILES as $file) {
		if (is_array($file["name"])) {
			/* Mehrere Dateien */
			$cnt = count($file["name"]);
			array_push($ftypes, $file["type"]);
			
			for($i2 = 0; $i2 < $cnt; $i2++) {
				$size = $file["size"][$i2];
				if ($size > 0 || !$file["error"][$i2]) {
					$dateipfad = $upload_dir.$file["name"][$i2];
					
					/* "DUPLICATE NAME" HANDLING */
					$itr = 2; // iteration
					while (file_exists($dateipfad)) {
						$dateipfad = $upload_dir.$file["name"][$i2].' ('.($itr++).')';
					}
					/* END "DUPLICATE NAME" HANDLING */
					
					array_push($files, $dateipfad);
					array_push($fsizes, $size);
					move_uploaded_file($file["tmp_name"][$i2], $dateipfad);
					
					$i = 0;
					$embed = 1;
					foreach ($myTypes as $typ) {
						if ($typ == substr($ftypes[0][$i2], 0, strlen($myTypes[$i]))) {
							$embed = 0;
							break;
						}
						$i++;
					}
					if (!$embed) array_push($ftags, $myTags[$i]);
					else array_push($ftags, "embed");
				}
			}
		}
	}

/* END FILE HANDLING */

if (isset($_POST["text"]) && trim($_POST["text"]) !== "") {
	echo "\n| ";
	
	$daten = array($_POST["text"], $_POST["name"]);
	
	$to_group = "";
	$to_user = "";
	if (substr($daten[0], 0, 1) == '#') {
		list($to_group) = explode(' ', $daten[0]);
		$to_group = substr($to_group, 1, strlen($to_group));
		$daten[0] = substr($daten[0], strlen($to_group) + 2, strlen($daten[0]));
	}
	if (substr($daten[0], 0, 1) == '@') {
		list($to_user) = explode(' ', $daten[0]);
		$to_user = substr($to_user, 1, strlen($to_user));
		$daten[0] = substr($daten[0], strlen($to_user) + 2, strlen($daten[0]));
	}
	
	$sql = $pdo->prepare("SELECT count(Timestamp) FROM spammer WHERE Username = ? AND Timestamp < curtime() - INTERVAL 5 SECOND");
	$sql->execute([$daten[1]]);
	$spam = $sql->fetchColumn();
	
	if ($spam != 0) {
		$sql = $pdo->prepare("DELETE FROM spammer WHERE Username = ?");
		$sql->execute([$daten[1]]);
		echo "Gelöscht / ";
	}
	
	// Bereinige Tabelle, wenn leer (Setze Index zurück) UND automatisch am nächsten Tag.
	$sql = $pdo->prepare("SELECT count(0) FROM spammer WHERE curdate() = date(Timestamp)");
	$sql->execute();
	$spam = $sql->fetchColumn();
	
	if ($spam == 0) {
		$sql = $pdo->prepare("TRUNCATE TABLE spammer");
		$sql->execute();
	}
	
	$sql = $pdo->prepare("SELECT count(Timestamp) FROM spammer WHERE Username = ?");
	$sql->execute([$daten[1]]);
	$spam = $sql->fetchColumn();
	if ($spam == 0) {
		// Schreibe Nachricht
		echo "Gesendet / ";
		$sql = $pdo->prepare("INSERT INTO messages (message, username, to_group, to_user, files, file_type, file_tag, file_size) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
		$sql->execute([$daten[0], $daten[1], $to_group, $to_user, json_encode($files), json_encode($ftypes), json_encode($ftags), json_encode($fsizes)]);
		
		// Warte 5 Sekunden
		$sql = $pdo->prepare("INSERT INTO spammer (Username) VALUES (?)");
		$sql->execute([$daten[1]]);
		
		if ($daten[0] == "Ping") {
			$sql = $pdo->prepare("INSERT INTO messages (message, username) VALUES('Pong', '(Server)')");
			$sql->execute();
		}
		else if ($daten[0] == "13") {
			$sql = $pdo->prepare("INSERT INTO messages (message, username) VALUES('Ah, die Glückszahl. Das ist ernst gemeint. ;-)', '(Server)')");
			$sql->execute();
		}
	}
	
	$sql = $pdo->prepare("OPTIMIZE TABLE messages");
	$sql->execute();
	echo "Warte";
}
?>